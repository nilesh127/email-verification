<?php
// ==============================================================================
//	Verify email while sign up 
// ==============================================================================

add_action( 'init', 'my_init' );
add_filter('woocommerce_registration_redirect', 'wc_registration_redirect');
add_filter('wp_authenticate_user', 'wp_authenticate_user',15,2);
add_action('user_register', 'my_user_register',15,2);

// prevents the user from logging in automatically after registering their account
function wc_registration_redirect( $redirect_to ) {     
    wp_logout();
    $url1 = get_site_url(). '/verify/?n=';
    wp_redirect($url1);                        
    exit;
}
// when the user logs in, checks whether their email is verified
function wp_authenticate_user( $userdata ) {            
    $has_activation_status = get_user_meta($userdata->ID, 'is_activated', false);
    if ($has_activation_status) {                          
        $isActivated = get_user_meta($userdata->ID, 'is_activated', true);
        if ( !$isActivated ) {
        	// resends the activation mail if the account is not activated
            my_user_register( $userdata->ID );              
            $userdata = new WP_Error(
                'my_theme_confirmation_error',
                __( '<strong>Error:</strong> Your account has to be activated before you can login. Please click the link in the activation email that has been sent to you.<br /> If you do not receive the activation email within a few minutes, check your spam folder or <a href="/verify/?u='.$userdata->ID.'">click here to resend it</a>.' )
            );
        }
    }
    return $userdata;
}
// when a user registers, sends them an email to verify their account
function my_user_register($user_id) { 
    $user_info = get_userdata($user_id);                                            
    $code = md5(time());                                                            
    $string = array('id'=>$user_id, 'code'=>$code);                                 
    update_user_meta($user_id, 'is_activated', 0);                                  
    update_user_meta($user_id, 'activationcode', $code);
    $url = get_site_url(). '/verify/?p=' .base64_encode( serialize($string));       
    $html = ( 'Please click this link '.$url.' to verify your email address and complete the registration process.' ); 
    wp_mail($user_info->user_email, __( 'Activate your Account' ), $html);          
}
// handles all this verification stuff
function my_init(){       
	// If accessed via an authentification link                          
    if(isset($_GET['p'])){                                                  
        $data = unserialize(base64_decode($_GET['p']));
        $code = get_user_meta($data['id'], 'activationcode', true);
        $isActivated = get_user_meta($data['id'], 'is_activated', true);    
        if( $isActivated ) {                                                
            wc_add_notice( __( 'This account has already been activated. Please log in with your username and password.' ), 'error' );
        }
        else {
            if($code == $data['code']){                                     
                update_user_meta($data['id'], 'is_activated', 1);           
                $user_id = $data['id'];                                     
                $user = get_user_by( 'id', $user_id ); 
                if( $user ) {
                    wp_set_current_user( $user_id, $user->user_login );
                    wp_set_auth_cookie( $user_id );
                    do_action( 'wp_login', $user->user_login, $user );
                }
                wc_add_notice( __( '<strong>Success:</strong> Your account has been activated! You have been logged in and can now use the site to its full extent.' ), 'notice' );
            } else {
                wc_add_notice( __( '<strong>Error:</strong> Account activation failed. Please try again in a few minutes or <a href="/verify/?u='.$userdata->ID.'">resend the activation email</a>.<br />Please note that any activation links previously sent lose their validity as soon as a new activation email gets sent.<br />If the verification fails repeatedly, please contact our administrator.' ), 'error' );
            }
        }
    }
    // If resending confirmation mail
    if(isset($_GET['u'])){                                          
        my_user_register($_GET['u']);
        wc_add_notice( __( 'Your activation email has been resent. Please check your email and your spam folder.' ), 'notice' );
    }
    // If account has been freshly created
    if(isset($_GET['n'])){                                          
        wc_add_notice( __( 'Thank you for creating your account. You will need to confirm your email address in order to activate your account. An email containing the activation link has been sent to your email address. If the email does not arrive within a few minutes, check your spam folder.' ), 'notice' );
    }
}